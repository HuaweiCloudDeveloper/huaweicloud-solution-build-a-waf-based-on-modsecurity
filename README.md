[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您在华为云弹性云服务器上基于开源ModSecurity软件，一键部署实现Web应用防火墙（WAF）功能。配合Nginx的灵活与高效，有效的增强Web安全性。ModSecurity是一个开源的、跨平台的Web应用防火墙（WAF）。它可以通过检查Web服务接收到的数据，以及发送出去的数据来对网站进行安全防护。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/build-a-waf-based-on-modsecurity.html](https://www.huaweicloud.com/solution/implementations/build-a-waf-based-on-modsecurity.html)

**架构图**
---------------
![方案架构](./document/build-a-WAF-based-on-modsecurity.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建一台Linux弹性云服务器，用于搭建Web应用防火墙（WAF）和Nginx负载均衡环境。
2. 在Linux弹性云服务器中安装配置Nginx，用于提供负载均衡能力。
3. 在Linux弹性云服务器中安装配置ModSecurity，用于提供Web应用防火墙（WAF）能力。
4. 创建弹性公网IP并绑定到服务器，用于提供访问公网和被公网访问能力。

**组织结构**
---------------
``` lua
huaweicloud-solution-build-a-WAF-based-on-modsecurity
├── build-a-WAF-based-on-modsecurity.tf.json -- 资源编排模板
├── userdata
    ├── configure_nginx_modsecurity.sh -- 脚本配置文件
```
**开始使用**
---------------
1、使用远程连接工具，登录负载均衡服务器，上传已有SSL证书（公钥、私钥）文件至指定目录：/usr/local/nginx/ssl/，上传请参考上传文件到云服务器方式概览，执行“cd /usr/local/nginx/sbin; ./nginx”命令，启动Nginx服务。

图1 SSL证书，启动Nginx服务

![ SSL证书，启动Nginx服务](./document/readme-image-001.png)

2、配置域名解析。网站解析将域名与虚拟IP挂载的公网IP地址相关联，实现通过在浏览器中直接输入域名访问网站。具体解析流程参考快速添加域名解析。

3、在浏览器中多次使用HTTP协议或HTTPS协议访问弹性公网IP或者域名，可以轮询访问后端业务服务器。如 http: //EIP、http: //域名、https: //EIP，https: //域名或直接输入域名。

图2 访问VIP挂载的EIP

![VIP挂载的EIP](./document/readme-image-002.png)

4、在浏览器中输入“https://弹性公网IP/?param=%22%3E%3Cscript%3Ealert(1);%3C/script%3E” 即可验证Web应用防火墙（WAF）是否生效。

图3 访问Web应用防火墙（WAF）

![访问Web应用防火墙](./document/readme-image-003.png)



